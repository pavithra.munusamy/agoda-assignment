package org.agoda;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AgodahomePage {
	static WebDriver driver;
	
	//public static void main(String[] args) {
		@BeforeClass
		public void Agodalaunch() {
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.agoda.com/?cid=1844104");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
		AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
		
		up.coupon();
		up.clickFlight();
		driver.close();
		
	//	AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
	//	UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
		
		
		}
	
		@Test(priority=1)
		public void onewaycase() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.Click_oneway();
			up.click_search();
			Thread.sleep(3000);
			up.click_Cross();
			driver.quit();
		}
		
		
		@Test(priority=2)
		public void roundTripcase() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.click_Round();
			up.click_search();
			Thread.sleep(3000);
			up.click_Cross();
			driver.quit();
			
		
		}	
		
		@Test(priority=3)
		public void onewaywitheconomycase() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.click_Round();
			up.click_economybtn();
			up.click_Economy();
			up.click_search();
			Thread.sleep(3000);
			up.click_Cross();
			driver.quit();
		
		}	
		
		@Test(priority=4)
		public void onewaywithepremiumeconomy() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.click_Round();
			up.click_economybtn();
			up.click_Premium();
			up.click_search();
			Thread.sleep(3000);
			up.click_Cross();
			driver.quit();
		
		}	
		
		
		@Test(priority=5)
		public void RoundTripwitheconomycase() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.click_Round();
			up.click_economybtn();
			up.click_Economy();
			up.click_search();
			Thread.sleep(3000);
			up.click_Cross();
			driver.quit();
		
		}
		
		@Test(priority=6)
		public void RoundTripwithpremiumeconomycase() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.click_Round();
			up.click_economybtn();
			up.click_Premium();
			up.click_search();
			Thread.sleep(3000);
			up.click_Cross();
			driver.quit();
		
		} 

		@Test(priority=7)
		public void Fromcase() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.Click_oneway();
			up.click_economybtn();
			up.click_Economy();
			up.click_from();
			up.select_newDelhi();
			up.click_To();
			up.select_Banglore();
			
			//up.click_Departure();
			up.click_Departuredatescurrent();
		
			up.click_Adult();
			up.click_Contiune();
			
			up.click_Room();
			
			up.click_search();
			
			//up.click_Cross();
			driver.quit();
		
		}

		@Test(priority=8)
		public void Roundtripcase() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.click_Round();
			up.click_economybtn();
			up.click_Economy();
			up.click_from();
			up.select_newDelhi();
			up.click_To();
			up.select_Banglore();
			
			//up.click_Departure();
			up.click_Departuredatescurrent();
			up.click_returncurrdate();
			Thread.sleep(2000);
			up.click_Adult();
			up.click_Contiune();
			
			up.click_Room();
			
			up.click_search();
			
			//up.click_Cross();
			driver.quit();
		}	
		
		@Test(priority=9)
		public void searchforoneway() throws InterruptedException {
			
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.agoda.com/?cid=1844104");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			UsingPojo up=PageFactory.initElements(driver, UsingPojo.class);
			AgodahomePage Agoda=PageFactory.initElements(driver, AgodahomePage.class);
			
			up.coupon();
			up.clickFlight();
			up.click_roundandoneway();
			up.Click_oneway();
			up.click_economybtn();
			up.click_Economy();
			up.click_from();
			up.select_newDelhi();
			up.click_To();
			up.select_Banglore();
		//  up.click_Departure();
		    up.click_Departuredatescurrent();
		//  up.click_returncurrdate();
		//	up.click_Departuredatesafter();
			up.click_Adult();
			up.click_Contiune();
			up.click_Room();
			up.click_Checkbox();
		    up.click_previousdaycheckin();
		//	up.click_checkoutdate();
	   //   up.click_Stayingat();
		//  up.click_banglore();
			up.click_search();
		//  up.click_Cross();
		//	driver.quit();
		}
		
		
		
}

